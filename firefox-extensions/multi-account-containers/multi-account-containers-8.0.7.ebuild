# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Lets you keep parts of your online life separated"
HOMEPAGE="https://github.com/mozilla/multi-account-containers"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3932862/${PN/-/_}-${PV}.xpi -> ${P}.zip"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
