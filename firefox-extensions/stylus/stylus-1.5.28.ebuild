# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Userstyles Manager"
HOMEPAGE="https://add0n.com/stylus.html"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3995806/styl_us-${PV}.xpi -> ${P}.zip"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
