# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Firefox Add-on that lets you open automatically managed disposable containers"
HOMEPAGE="https://github.com/stoically/temporary-containers"
SRC_URI="https://github.com/stoically/${PN}/releases/download/v${PV}/${PN/-/_}-${PV}-fx.xpi -> ${P}.zip"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
