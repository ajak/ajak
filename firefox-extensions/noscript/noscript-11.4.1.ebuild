# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="The popular NoScript Security Suite browser extension"
HOMEPAGE="https://noscript.net/"
SRC_URI="https://github.com/hackademix/${PN}/releases/download/${PV}/${P}.xpi -> ${P}.zip"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
