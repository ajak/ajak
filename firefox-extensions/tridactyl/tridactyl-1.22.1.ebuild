# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="A Vim-like interface for Firefox, inspired by Vimperator/Pentadactyl."
HOMEPAGE="https://tridactyl.xyz/"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3926466/${PN}_vim-${PV}.xpi -> ${P}.zip"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
