# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Browserpass is a browser extension for zx2c4's pass"
HOMEPAGE="https://github.com/browserpass/browserpass-extension"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3711209/${PN}_ce-${PV}.xpi -> ${P}.zip"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"

RDEPEND="www-plugins/browserpass"
