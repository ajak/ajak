# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Add ability to lock specific tree as collapsed"
HOMEPAGE="https://github.com/piroor/tst-lock-tree-collapsed"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3939894/${PN/-/_}-${PV}.xpi -> ${P}.zip"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
