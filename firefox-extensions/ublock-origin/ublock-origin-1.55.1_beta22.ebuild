# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

MY_PV="${PV/_beta/b}"
MY_PV="${MY_PV/_rc/rc}"
MY_P="uBlock0_${MY_PV}"

DESCRIPTION="An efficient blocker for Chromium and Firefox."
HOMEPAGE="https://ublockorigin.com/"
SRC_URI="https://github.com/gorhill/uBlock/releases/download/${MY_PV}/${MY_P}.firefox.signed.xpi -> ${P}.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
