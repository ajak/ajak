# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Greasemonkey is a user script manager for Firefox"
HOMEPAGE="https://www.greasespot.net/"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/3716451/${P}.xpi -> ${P}.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
