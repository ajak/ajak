# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="Show tabs like a tree"
HOMEPAGE="https://piro.sakura.ne.jp/xul/_treestyletab.html.en"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/4028300/${PN/-/_}-${PV}.xpi -> ${P}.zip"

# PSFL? https://github.com/piroor/treestyletab/blob/3.9.10/COPYING.txt#L11
LICENSE="MPL-2.0 MPL-1.1 MIT" # PSFL-2.4"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
