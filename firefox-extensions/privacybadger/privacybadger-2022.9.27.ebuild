# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit firefox-ext

DESCRIPTION="A browser extension that automatically learns to block invisible trackers."
HOMEPAGE="https://privacybadger.org"
SRC_URI="https://addons.mozilla.org/firefox/downloads/file/4008174/privacy_badger17-${PV}.xpi -> ${P}.zip"

# TODO: plus whatever this is: https://github.com/EFForg/privacybadger/blob/master/LICENSE#L89
LICENSE="MIT Unlicense GPL-3 Apache-2.0 CC-BY-SA-3.0"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
