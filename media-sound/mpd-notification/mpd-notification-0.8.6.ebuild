# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Notify about tracks played by mpd"
HOMEPAGE="https://github.com/eworm-de/mpd-notification"
SRC_URI="https://github.com/eworm-de/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"

# TODO: audit deps more
DEPEND="dev-libs/glib:2
	dev-libs/iniparser:4
	media-libs/libmpdclient
	media-video/ffmpeg:=
	sys-apps/file
	sys-apps/systemd:=
	x11-libs/gdk-pixbuf:2
	x11-libs/libnotify"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=( "${FILESDIR}/${P}-no-markdown.patch" )
