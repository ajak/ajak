# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Ansible Community General Collection - ajak patches"
HOMEPAGE="https://github.com/ajakk/community.general"
EGIT_REPO_URI="https://github.com/ajakk/community.general"
EGIT_BRANCH="portage-update"

LICENSE="GPL-3"
SLOT="0"
RESTRICT="mirror"

src_install() {
	insinto "/usr/share/ansible/plugins/modules/"
	doins plugins/modules/packaging/os/portage.py
}
