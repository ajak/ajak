# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="A tool to aid in Gentoo development and administration, via podman"
HOMEPAGE="https://github.com/ajakk/podman-gentoo"
EGIT_REPO_URI="https://github.com/ajakk/${PN}.git"

LICENSE="GPL-3+"
SLOT="0"
RESTRICT="mirror"

src_install() {
	exeinto /usr/bin
	newexe "${PN}.sh" "${PN}"
}
