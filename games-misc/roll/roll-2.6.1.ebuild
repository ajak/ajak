# Copyright 2020-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="roll dices specified in a simple and intuitive way"
HOMEPAGE="https://matteocorti.github.io/roll/"
SRC_URI="https://github.com/matteocorti/roll/releases/download/v${PV}/${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
RESTRICT="mirror"
