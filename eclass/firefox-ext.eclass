# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: firefox-ext.eclass
# @MAINTAINER:
# John Helmert III <ajak@gentoo.org>
# @AUTHOR:
# John Helmert III <ajak@gentoo.org>
# @SUPPORTED_EAPIS: 8
# @BLURB: Eclass used to install Firefox extensions
# @DESCRIPTION:
# Eclass used to install Firefox extensions

if [[ ! ${_FIREFOX_EXT_ECLASS} ]]; then
_FIREFOX_EXT_ECLASS=1

case ${EAPI} in
	8) ;;
	*) die "${ECLASS}: EAPI ${EAPI} unsupported."
esac

[[ ${CATEGORY} == firefox-extensions ]] ||
	die "Ebuild error: this eclass can only be used in the firefox-extensions category!"

S="${WORKDIR}"

# xpi's are all zip's in disguise, but some things are distributed as
# zipfiles anyway
BDEPEND="app-arch/unzip
	app-misc/jq"

EXPORT_FUNCTIONS src_install

firefox-ext_src_install() {
	local jq_query ext_id dest

	jq_query=(
		". | {browser_specific_settings,applications} |"
		".[].gecko.id | select(. != null)"
	)

	extension_id=$(jq --raw-output "${jq_query[*]}" manifest.json | sort --unique)
	[[ -n "${extension_id}" ]] ||
		die "Look's there's no application ID in extension's manifest.json!"

	dest="${ED}/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/${extension_id}"

	mkdir -p "${dest}" || die
	cp -pr "${WORKDIR}"/* "${dest}" || die
}

fi
